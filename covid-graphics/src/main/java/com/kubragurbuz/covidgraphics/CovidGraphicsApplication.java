package com.kubragurbuz.covidgraphics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class CovidGraphicsApplication extends SpringBootServletInitializer {
  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(CovidGraphicsApplication.class);
  }
	public static void main(String[] args) {
		SpringApplication.run(CovidGraphicsApplication.class, args);
	}

}
