package com.kubragurbuz.covidgraphics.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

  @GetMapping("/viewBooks")
  public String viewBooks(Model model) {
    model.addAttribute("books", "Book Kubra Test");
    return "test";
  }

}
